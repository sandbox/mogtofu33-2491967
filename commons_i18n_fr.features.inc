<?php
/**
 * @file
 * commons_i18n_fr.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function commons_i18n_fr_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_message_type_alter().
 */
function commons_i18n_fr_default_message_type_alter(&$defaults) {
  if (isset($defaults['commons_like_user_likes_node'])) {
    $defaults['commons_like_user_likes_node']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture]',
        'format' => 'full_html',
        'safe_value' => '[message:user:picture]',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> aime [message:field-target-nodes:0:type] <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> aime [message:field-target-nodes:0:type] <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
    );
  }
  if (isset($defaults['commons_q_a_question_answered'])) {
    $defaults['commons_q_a_question_answered']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a répondu à la question <em>[message:field-target-nodes:0:field-related-question:title]</em> par "<a href="[message:field-target-nodes:0:field-related-question:url]#node-[message:field-target-nodes:0:nid]">[message:field-target-nodes:0:title_field]</a>"',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a répondu à la question  <em>[message:field-target-nodes:0:field-related-question:title]</em> par "<a href="[message:field-target-nodes:0:field-related-question:url]#node-[message:field-target-nodes:0:nid]">[message:field-target-nodes:0:title_field]</a>"',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'full_html',
        'safe_value' => '[commons-groups:in-groups-text]',
      ),
    );
  }
  if (isset($defaults['commons_q_a_question_asked'])) {
    $defaults['commons_q_a_question_asked']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a posé la question <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a posé la question <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'full_html',
        'safe_value' => '[commons-groups:in-groups-text]',
      ),
    );
  }
  if (isset($defaults['commons_posts_post_created'])) {
    $defaults['commons_posts_post_created']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a publié <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a publié <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'filtered_html',
      ),
    );
  }
  if (isset($defaults['commons_wikis_wiki_updated'])) {
    $defaults['commons_wikis_wiki_updated']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'full_html',
        'safe_value' => '[message:user:picture:35x35]',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a mis à jour la page wiki <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a mis à jour la page wiki <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'full_html',
        'safe_value' => '[commons-groups:in-groups-text]',
      ),
    );
  }
  if (isset($defaults['commons_follow_user_user_followed'])) {
    $defaults['commons_follow_user_user_followed']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> suit désormais <a href="[message:field-target-users:0:url]">[message:field-target-users:0:name]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> suit désormais <a href="[message:field-target-users:0:url]">[message:field-target-users:0:name]</a>',
      ),
    );
  }
  if (isset($defaults['trusted_contact_request_approved'])) {
    $defaults['trusted_contact_request_approved']->message_text['fr'] = array(
      0 => array(
        'value' => '<p>[message:field-approving-user:name] a approuvé votre demande de contact authentifié.</p>',
        'format' => 'full_html',
        'safe_value' => '<p>[message:field-approving-user:name] a approuvé votre demande de contact authentifié.</p>',
      ),
      1 => array(
        'value' => '<p>[message:field-approving-user:name] a approuvé votre demande de contact authentifié.</p><p>En savoir plus :&nbsp;<span style="font-size: 13.333333015441895px;">[message:field-approving-user:url:absolute]</span></p>',
        'format' => 'full_html',
        'safe_value' => '<p>[message:field-approving-user:name] a approuvé votre demande de contact authentifié.</p><p>En savoir plus :&nbsp;<span style="font-size: 13.333333015441895px;">[message:field-approving-user:url:absolute]</span></p>',
      ),
    );
  }
  if (isset($defaults['trusted_contact_request_pending'])) {
    $defaults['trusted_contact_request_pending']->message_text['fr'] = array(
      0 => array(
        'value' => '<p>Vous avez une demande de contact à authentifier en attente de [message:field-requesting-user:name]</p>',
        'format' => 'full_html',
        'safe_value' => '<p>Vous avez une demande de contact à authentifier en attente de [message:field-requesting-user:name]</p>',
      ),
      1 => array(
        'value' => '<p>Bonjour,</p>
    <p>Vous avez une demande de contact à authentifier en attente de [message:field-requesting-user:name]</p>
    <p>Vous pouvez la valider ou ignorer cette demande</p>
    <p><a href="@{approve-url}">Approve</a> | <a href="@{ignore-url}">Ignore</a></p>',
        'format' => 'full_html',
        'safe_value' => '<p>Bonjour,</p>
    <p>Vous avez une demande de contact à authentifier en attente de [message:field-requesting-user:name]</p>
    <p>Vous pouvez la valider ou ignorer cette demande</p>
    <p><a href="@{approve-url}">Approve</a> | <a href="@{ignore-url}">Ignore</a></p>',
      ),
    );
  }
  if (isset($defaults['commons_notify_comment_created'])) {
    $defaults['commons_notify_comment_created']->message_text['fr'] = array(
      0 => array(
        'value' => 'Nouveau commentaire sur [message:field-target-nodes:0:title_field] dans [message:field-target-nodes:0:og-group-ref:0:title]: [message:field-target-nodes:0:title_field]',
        'format' => 'plain_text',
        'safe_value' => '<p>Nouveau commentaire sur [message:field-target-nodes:0:title_field] dans [message:field-target-nodes:0:og-group-ref:0:title]: [message:field-target-nodes:0:title_field]</p>',
      ),
      1 => array(
        'value' => 'Bonjour [message:user:name],

    [message:field-target-comments:0:author] a commenté sur [message:field-target-nodes:0:title_field] [commons-groups:in-groups-text]:

    [message:field-target-comments:0:body]

    Lire et répondre : [message:field-target-comments:0:url]
    Mettre à jour vos paramètres de notification : [site:url]/user/[message:user:uid]/notification-settings',
        'format' => 'plain_text',
        'safe_value' => '<p>Bonjour [message:user:name],</p>
    <p>[message:field-target-comments:0:author] a commenté sur [message:field-target-nodes:0:title_field] [commons-groups:in-groups-text]:</p>
    <p>[message:field-target-comments:0:body]</p>
    <p>Lire et répondre : [message:field-target-comments:0:url]<br />
    Mettre à jour vos paramètres de notification : [site:url]/user/[message:user:uid]/notification-settings</p>',
      ),
    );
  }
  if (isset($defaults['commons_notify_comment_created_no_groups'])) {
    $defaults['commons_notify_comment_created_no_groups']->message_text['fr'] = array(
      0 => array(
        'value' => 'ouveau commentaire sur [message:field-target-nodes:0:title_field] ',
        'format' => 'plain_text',
        'safe_value' => '<p>Nouveau commentaire sur [message:field-target-nodes:0:title_field]</p>',
      ),
      1 => array(
        'value' => 'Bonjour [message:user:name],

    [message:field-target-comments:0:author] a commenté sur [message:field-target-nodes:0:title_field]:

    [message:field-target-comments:0:body]

    Lire et répondre : [message:field-target-comments:0:url]
    Mettre à jour vos paramètres de notification : [site:url]/notification-settings',
        'format' => 'plain_text',
        'safe_value' => '<p>Bonjour [message:user:name],</p>
    <p>[message:field-target-comments:0:author] a commenté sur [message:field-target-nodes:0:title_field]:</p>
    <p>[message:field-target-comments:0:body]</p>
    <p>Lire et répondre : [message:field-target-comments:0:url]<br />
    Mettre à jour vos paramètres de notification : [site:url]/notification-settings</p>',
      ),
    );
  }
  if (isset($defaults['commons_notify_node_created'])) {
    $defaults['commons_notify_node_created']->message_text['fr'] = array(
      0 => array(
        'value' => 'Nouveau [message:field-target-nodes:0:content-type] sur [message:field-target-nodes:0:og-group-ref:0:title]: [message:field-target-nodes:0:title_field]',
        'format' => 'plain_text',
        'safe_value' => '<p>Nouveau [message:field-target-nodes:0:content-type] sur [message:field-target-nodes:0:og-group-ref:0:title]: [message:field-target-nodes:0:title_field]</p>',
      ),
      1 => array(
        'value' => 'Bonjour [message:user:name],

    [message:field-target-nodes:0:author] a créé le [message:field-target-nodes:0:content-type] “[message:field-target-nodes:0:title_field]” sur [site:name] [commons-groups:in-groups-text] le [message:field-target-nodes:0:created]:

    [message:field-target-nodes:0:body]


    Lien permanent : [message:field-target-nodes:0:url]
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form

    Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings',
        'format' => 'plain_text',
        'safe_value' => '<p>Bonjour [message:user:name],</p>
    <p>[message:field-target-nodes:0:author] a créé le [message:field-target-nodes:0:content-type] “[message:field-target-nodes:0:title_field]” sur [site:name] [commons-groups:in-groups-text] le [message:field-target-nodes:0:created]:</p>
    <p>[message:field-target-nodes:0:body]</p>
    <p>Lien permanent : [message:field-target-nodes:0:url]<br />
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form</p>
    <p>Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings</p>',
      ),
    );
  }
  if (isset($defaults['commons_notify_node_created_no_groups'])) {
    $defaults['commons_notify_node_created_no_groups']->message_text['fr'] = array(
      0 => array(
        'value' => 'Nouveau [message:field-target-nodes:0:content-type] sur [site:name]: [message:field-target-nodes:0:title_field]',
        'format' => 'plain_text',
        'safe_value' => '<p>Nouveau [message:field-target-nodes:0:content-type] sur [site:name]: [message:field-target-nodes:0:title_field]</p>',
      ),
      1 => array(
        'value' => 'Bonjour [message:user:name],

    [message:field-target-nodes:0:author] a créé le [message:field-target-nodes:0:content-type] “[message:field-target-nodes:0:title_field]” sur [site:name] le [message:field-target-nodes:0:created]:

    [message:field-target-nodes:0:body]


    Lien permanent : [message:field-target-nodes:0:url]
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form

    Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings',
        'format' => 'plain_text',
        'safe_value' => '<p>Bonjour [message:user:name],</p>
    <p>[message:field-target-nodes:0:author] a créé le [message:field-target-nodes:0:content-type] “[message:field-target-nodes:0:title_field]” sur [site:name] le [message:field-target-nodes:0:created]:</p>
    <p>[message:field-target-nodes:0:body]</p>
    <p>Lien permanent : [message:field-target-nodes:0:url]<br />
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form</p>
    <p>Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings</p>',
      ),
    );
  }
  if (isset($defaults['commons_activity_streams_comment_created'])) {
    $defaults['commons_activity_streams_comment_created']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a commenté sur <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a commenté sur <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'full_html',
        'safe_value' => '[commons-groups:in-groups-text]',
      ),
    );
  }
  if (isset($defaults['commons_activity_streams_node_created'])) {
    $defaults['commons_activity_streams_node_created']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a créé <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a créé <a href="[message:field-target-nodes:0:url]">[message:field-target-nodes:0:title_field]</a>',
      ),
      2 => array(
        'value' => '[commons-groups:in-groups-text]',
        'format' => 'full_html',
        'safe_value' => '[commons-groups:in-groups-text]',
      ),
    );
  }
  if (isset($defaults['commons_activity_streams_user_profile_updated'])) {
    $defaults['commons_activity_streams_user_profile_updated']->message_text['fr'] = array(
      0 => array(
        'value' => '[message:user:picture:35x35]',
        'format' => 'filtered_html',
      ),
      1 => array(
        'value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a mis à jour son profil',
        'format' => 'full_html',
        'safe_value' => '<a href="[message:user:url:absolute]">[message:user:name]</a> a mis à jour son profil',
      ),
    );
  }
  if (isset($defaults['commons_events_event_node_created'])) {
    $defaults['commons_events_event_node_created']->message_text['fr'] = array(
      0 => array(
        'value' => 'Nouvel évènement sur [site:name]: [message:field-target-nodes:0:title_field]',
        'format' => 'plain_text',
        'safe_value' => '<p>Nouvel évènement sur [site:name]: [message:field-target-nodes:0:title_field]</p>',
      ),
      1 => array(
        'value' => 'Bonjour [message:user:name],

    [message:field-target-nodes:0:author] a créé le nouvel évènement “[message:field-target-nodes:0:title_field]” sur [site:name] [commons-groups:in-groups-text] le [message:field-target-nodes:0:created]:

    Quoi : [message:field-target-nodes:0:title_field]
    Quand : [message:field-target-nodes:0:field_date]
    Où (si applicable) : [message:field-target-nodes:0:field_address]

    [message:field-target-nodes:0:body]


    Lien permanent : [message:field-target-nodes:0:url]
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form

    Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings',
        'format' => 'plain_text',
        'safe_value' => '<p>Bonjour [message:user:name],</p>
    <p>[message:field-target-nodes:0:author] a créé le nouvel évènement “[message:field-target-nodes:0:title_field]” sur [site:name] [commons-groups:in-groups-text] le [message:field-target-nodes:0:created]:</p>
    <p>Quoi : [message:field-target-nodes:0:title_field]<br />
    Quand : [message:field-target-nodes:0:field_date]<br />
    Où (si applicable) : [message:field-target-nodes:0:field_address]</p>
    <p>[message:field-target-nodes:0:body]</p>
    <p>Lien permanent : [message:field-target-nodes:0:url]<br />
    Ajouter un commentaire : [message:field-target-nodes:0:url]#comment-form</p>
    <p>Mettre à jour vos paramètres de notification : [site:url]user/[message:user:uid]/notification-settings</p>',
      ),
    );
  }
}
