
-------------------------------------------------------------------------------
Commons i18n support for Drupal 7.x
  by Jean Valverde - http://developpeur-drupal.com
-------------------------------------------------------------------------------

DESCRIPTION:
This module add some feature and settings to get a better i18n support in Drupal
Commons 3.x
Include commons_i18n_fr module to add French translations.

-------------------------------------------------------------------------------

INSTALLATION:
* Put the module in your Drupal modules directory and enable it in 
  admin/modules, enable commons_i18n_fr if needed then:
  * Copy jquery.timeago.fr.js from translations folder of this module in 
    profiles/commons/libraries/timeago
  * Go to language translation Configuration > Translate > Import
    import translations/commons.fr.po in interface and
    translations/views.fr.po in views group.
* Apply Entity patch: https://www.drupal.org/node/1932530#comment-8931849
-------------------------------------------------------------------------------
